# ALLEZLUA
Transpiler du langage Oberon vers le langage Go, écrit en Lua.
## Nom
"Allez Lua" (Alléluia) clin d'oeil au fait que Lua est développé à l'université PUC-Rio ;-) <BR>
Traduit littéralement vers l'anglais cela donne '**G**o **L**ua' => 'GL' + '**O**'beron + '**T**'ranspiler => GLO**T** , GLO**P** (parser) , GLO**S** (scanner) , GLO**G** (generator)<BR>
Si la compilation échoue, le transpiler émet un message précisant la cause précédée de l'interjection ['_Pas glop, pas glop !_'](https://fr.wikipedia.org/wiki/Pifou) et bien entendu, en cas de réussite, 'Glop, glop !'  

## Utilisation 
lua GLOT.lua  <un_module_source.mod>

## Description
Mon objectif principal est de comprendre les algorithmes et le code décrits dans le livre du professeur WIRTH 'Compiler Construction'.<BR>
Après quelques tentatives frustrantes pour transposer fidèlement le code en Java, puis en C, j'ai pensé qu'il serait plus judicieux d'utiliser un langage interprété "moderne", type Lua ou Python.  <BR>
Lua est à la fois puissant et très léger, et va permettre de se détacher en partie du code original et le simplifier en utilisant les facilités de ce langage décrites dans l'excellent livre "Programming in Lua". Un effort particulier sera fait pour utiliser des noms de variables plus explicites que dans le code original et à le commenter. <BR>
Un second objectif est de séparer le processus de compilation en deux passes en suivant les indications de WIRTH lui-même (cf Compiler Construction chapitre 16.5), ceci permettant de cibler différents langages ou architectures de processeurs, en ne changeant que la partie "backend".<BR>

Lors de la première passe (front-end) ce [transpiler ](https://fr.wikipedia.org/wiki/Compilateur_source\_%C3%A0_source)écrira [l'arbre syntaxique](https://fr.wikipedia.org/wiki/Arbre_de_la_syntaxe_abstraite) et la [table des symboles](https://fr.wikipedia.org/wiki/Table_des_symboles) dans un fichier, lui même en langage Lua par la technique de la [sérialisation](https://www.lua.org/pil/12.1.html). La seconde passe (back-end) rechargera ce fichier (AST) et générera les codes cibles, OBERON et GO.<BR>

## Feuille de route
- Phase 1 :  A chacune des itérations correspond un enrichissement de la grammaire afin atteindre l'objectif d'analyser le code source aux niveaux lexical et syntaxique de manière à construire la table des symboles et celle de l'AST, lesquelles seront exportées sous forme de fichier Lua (sérialisation). La clause IMPORT sera implémentée.<BR>
- Phase 2 : A partir des deux fichiers générés à l'étape précédente, on va regénérer , en langage OBERON, pour contrôler que le code transpilé, soumis à un compilateur comme par exemple [Norebo](https://github.com/pdewacht/project-norebo), fourni bien à l'exécution le même résultat que le code source initial.<BR>
- Phase 3 : transpilation vers le langage GO.<BR>
- Phase 4 : Le compilateur de N. WIRTH n'utilise pas, à dessein, toutes les possibilités du langage, par exemple les réels ou les extensions objet. Par ajouts successifs on étend la grammaire du langage pour atteindre l'auto-compilation du compilateur  (ajout de POINTER , LONGINT ...).<BR>
- Phase 5 : enrichissement de la grammaire jusqu'à permettre la compilation du langage OBERON-7 (extension Objet)<BR>

## Bibiographie (et remerciements pour leur pédagogie)
Prof N. WIRTH         : https://people.inf.ethz.ch/wirth/CompilerConstruction/ <BR>
Régis CRELIER         : https://wiki.oberon.org/_media/blackbox/crelier_1991.pdf <BR>
Paul REED             : http://www.projectoberon.com/about <BR>
Peter De Wachter      : https://github.com/pdewacht/project-norebo <BR>
Dick PURNELL          : https://sites.google.com/site/dicknewsite/home/computing/byte-articles/the-oberon-language  <BR>
Roberto Ierusalimschy                    : Programming in LUA <BR>
W.P. Salman, O. Tisserand, B. Toulout    : FORTH (édition en français chez EYROLLES)  <BR>
The Jupiter Ace by Jupiter Cantab        : https://en.wikipedia.org/wiki/Jupiter_Ace <BR>

## Statut du projet 
Actif (novembre 2024) : voir les détails dans le [Wiki](https://framagit.org/Lunes/allezlua/-/wikis/home)
