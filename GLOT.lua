local version = "'GLOT' Go Lua Oberon Transpiler  :  version 1.7 (Etape 1, iteration 7, version en cours) du 24 novembre 2024"
  print(version) 
-- Variables globales visibles dans tous les modules chargés (OSS, GLOT)
cc = '\7'               -- caractère courant initialisé
symbole = null  		   	-- symbole courant
identifiant = ''		    -- identifiant courant
identifiant_prec = ''	  -- identififiant précédent
nombre = '';			      -- nombre courant
ObjectList = nil        -- point d'entrée niveau MODULE

 
local OSG = require "GLOG" ; --print(OSG.version)
local OSP = require "GLOP" ; --print(OSP.version)

-- main 
if arg[1] ~= nil then
      if arg[2] ~= nil then
          f = io.open(arg[2])
          if f ~= nil then
              io.write("fichier de sortie existant :  le remplacer ? [y/n]: ")
              confirm = io.read()
              if confirm ~= "y" then return end
          end
          io.output(arg[2])
      end
else 
   arg[1] = "./Jeux_de_test/GLOT_test_1.7.mod"
end

fic_source = io.open(arg[1])
print(arg[1])

OSP.Compile(fic_source)
fic_source:close()


--EXPORT
ObjectList = nil  -- point d'entrée niveau MODULE
OSP.newObject("N","CONST","INTEGER",10)
OSP.newObject("T","TYPE","ARRAY",10, "INTEGER")
OSP.newObject("R","TYPE","RECORD", {f="INTEGER",g="INTEGER",tableau="T"})
OSP.newObject("x","VAR","INTEGER")OSP.newObject("x","VAR","INTEGER")
OSP.newObject("y","VAR","T")

--IMPORT 
--OSG.Import()
os.exit(0)