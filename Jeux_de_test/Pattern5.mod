(*Pattern 5: Boolean expressions, If statements: Conditional statements imply that parts of them are
skipped. This is done by the use of branch instructions whose operand specifies the distance of the
branch. The instructions refer to the condition-register as an implicit operand. Its value is
determined by a preceding instruction, typically a compare or a bit-test instruction.
The Boolean operators & and OR are purposely not defined as total functions, but rather by the
equations
p & q = if p then q else FALSE
p OR q = if p then TRUE else q
Consequently, Boolean operators must be translated into branches too. Evidently, branches
stemming from if statements and branches stemming from Boolean operators should be merged, if
possible. The resulting code therefore does not necessarily mirror the structure of the if statement
directly, as can be seen from the code in Pattern5. We must conclude that code generation for
Boolean expressions differs in some aspects from that for arithmetic expressions.
The example of Pattern5 is also used to exhibit the code resulting from the standard procedures
INC, DEC, INCL, and EXCL. These procedures provide an opportunity to use shorter code in those
cases where a single two-operand instruction suffices, i.e. when one of the arguments is identical
with the destination.
*)
MODULE Pattern5;
VAR n: INTEGER; s: SET;       --  0,4
BEGIN
IF n = 0 THEN                 --  LDR R0 SB 0
                              --  CMP R0 R0 0
                              --  BNE 3
  INC(n)                      --  LDR R0 SB 0
                              --  ADD R0 R0 1
                              --  STR R0 SB 0
END ;
IF (n >= 0) & (n < 100) THEN  --  LDR SB R0 ...
                              --  LDR R0 SB 0 (n)
                              --  CMP R0 R0 0
                              --  BLT 6
                              --  LDR R0 SB 0
                              --  CMP R0 R0 100
                              --  BGE 3
  DEC(n)                      --  LDR R0 SB 0
                              --  SUB R0 R0 1
                              --  STR R0 R0 0
END ;
IF ODD(n) OR (n IN s) THEN    --  LDR SB R0 ...
                              --  LDR R0 SB 0 (n)
                              --  AND R0 R0 1
                              --  BNE 5
                              --  LDR R0 SB 4 (s)
                              --  LDR R1 SB 0
                              --  ADD R1 R1 1
                              --  ROR R0 R0 R1
                              --  BPL 2
  n := -1000                  --  MOV R0 R0 -1000
                              --  STR R0 SB 0
END ;
IF n < 0 THEN                 --  LDR SB R0 ...
                              --  LDR R0 SB 0
                              --  CMP R0 R0 0
                              --  BGE 3
  s := {}                     --  MOV R0 R0 0 {}
                              --  STR R0 SB 4
                              --  B 17
ELSIF n < 10 THEN             --  LDR SB R0 ...
                              --  LDR R0 SB 0
                              --  CMP R0 R0 10
                              --  BGE 3
  s := {0}                    --  MOV R0 R0 1
                              --  STR R0 SB 4
                              --  B 10
ELSIF n < 100 THEN            --  LDR SB R0 ...
                              --  LDR R0 SB 0
                              --  CMP R0 R0 100
                              --  BGE 3
  s := {1}                    --  MOV R0 R0 2
                              --  STR R0 SB 4
                              --  B 3
ELSE
  s := {2}                    --  MOV R0 R0 4
                              --  LDR SB R0 ...
                              --  STR R0 SB 4
END
END Pattern5