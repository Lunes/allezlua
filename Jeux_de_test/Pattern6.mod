(*
Pattern 6: While and repeat statements.
*)
MODULE Pattern6;
VAR i: INTEGER;
BEGIN i := 0;         --  MOV R0 R0 0
                      --  STR R0 SB 0
WHILE i < 10 DO       --  LDR SB R0 ...
                      --  LDR R0 SB 0
                      --  CMP R0 R0 10
                      --  BGE 4
  i := i + 2          --  LDR R0 SB 0
                      --  ADD R0 R0 2
                      --  STR R0 SB 0
END ;                 --  B -8
REPEAT i := i - 1     --  LDR SB R0 ...
                      --  LDR R0 SB 0
                      --  SUB R0 R0 1
                      --  STR R0 SB 0
UNTIL i = 0           --  LDR R0 SB 0
                      --  CMP R0 R0 0
                      --  BNE -7
END Pattern6.
