(* Pattern 7: For statements. *)
MODULE Pattern7;
VAR i, m, n: INTEGER;
BEGIN
 FOR i := 0 TO n-1          --  DO MOV R0 R0 0
                            --  LDR R1 SB 8
                            --  SUB R1 R1 1
                            --  CMP LNK R0 R1
                            --  BGT 7
                            --  STR R0 SB 0
 m := 2*m                   --  LDR R0 SB 4
                            --  LSL R0 R0 1
                            --  STR R0 SB 4
END                         --  LDR R0 SB 0
                            --  ADD R0 R0 1
                            --  B -11
END Pattern7