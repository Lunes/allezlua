```
          integer = digit  {digit}
          ident = letter {letter | digit}.
          type = ident.
          factor= ident
          IdentList = ident {"," ident}.
          Declarations = ["CONST" {ident"=" factor ";"}]
                         ["TYPE" {ident":" type ";"}]
                         ["VAR" {IdentList ":" type ";"}]
          import = ident [":=" ident]
          ImportList = "IMPORT" import {"," import}.
          module = "MODULE" ident ";" [ImportList] Declarations "END" ident "."  

MODULE GLOT_test; -- Valide à partir de GLOT version 1.7 et ultérieures

IMPORT module1.

CONST 	(* un commentaire multilignes :
			les constantes de type chaîne seront vus dans une version ultérieure
		*)

  const_int1	= 1;
  const_int2  = 200;
--  arobase       = '@'
--  version       = "version 5012"

(* on va corser 
              (* récursion (*   à la "Inception"  *)  *)

*)
(* 
TYPE  
  car_type1                       : CHAR; 
  int_type1                       : INTEGER; 
  bool_type1                      : BOOLEAN; 

*)

VAR 
  car1 ,car2 , car3       : CHAR;
  int1 ,int2 , int3       : INTEGER;



(* types définis à voir dans une prochaine itération  
  car4                   : car_type1; 
  int1                   : int_type1; 
*)


BEGIN 
(* ce code sera exécuté au chargement du module *)
  int1 := const_int1;
  int2 := const_int2;
  int3 := int1 + int2;

END GLOT_test.

