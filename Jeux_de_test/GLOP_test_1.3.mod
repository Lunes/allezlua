Création de types simples renvoyant aux types de bases ou précédemment définis 

 integer = digit  {digit}
 ident = letter {letter | digit}.
 type = ident.
 IdentList = ident {"," ident}.
 Declarations = ["TYPE" {ident":" type ";"}]
                ["VAR" {IdentList ":" type ";"}]
 module = "MODULE" ident ";" Declarations "END" ident "."  

MODULE GLOP_test; -- doit être compilable par la version 1.3 et ultérieures
TYPE 
  car_type1                       : CHAR; 
  int_type1                       : INTEGER; 
  bool_type1                      : BOOLEAN; 
(*
  car_type2,car_type3, car_type4  : CHAR;
  int_type2, int_type3, int_type4 : INTEGER; 
  bool_type2 ,bool_type3          : BOOLEAN;
*)

VAR 
  car2 ,car3 ,car4       : CHAR;
(*  car1                   : car_type1; 
  int1                   : int_type1; 
  int2 ,int3 , int4      : int_type2;
*)
--BEGIN

END GLOP_test.
_______________________________________________________________
code utilisé :

-- Constantes globales
null_ = 0
MODULE_ = 69
ident_ = 31
point_virgule_ = 52
END_ = 53
point_ = 18
virgule_ = 40
VAR_   = 65
deux_points_ = 41

-- Symboles lexicaux d'Oberon 
symboles = { 
    null_ = 0, point_ = 18, CHAR = 20, INTEGER = 21, ident_ = 31, comma_ = 40, deux_points_ = 41,
    point_virgule_ = 52 , VAR = 65 , END = 53 , MODULE = 69 ,
}


-- Variables globales
cc = ''				-- caractère courant
symbole = null_			-- symbole courant
identifiant = ''		-- identifiant courant


function Identifiant() 
... 

  if mots_clefs[identifiant] then
    io.write(" qui est un mot clef et son code est ",symboles[identifiant], ' ')
    return symboles[identifiant]
  else
    return ident_
  end
...

--- test  
lexemes  = Set{";", ">=", "~", ":=", ":", } 
for k,v in pairs(lexemes) do  print(k,v) end

