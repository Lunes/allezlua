(* Pattern 14: Record extensions as VAR parameters: Records occurring as VAR-parameters may
also require a type test at program execution time. This is because VAR-parameters effectively
constitute hidden pointers. Type tests and type guards on VAR-parameters are handled in the
same way as for variables referenced via pointers, with a slight difference, however. Statically
declared record variables may be used as actual parameters, and they are not prefixed by a type
tag. Therefore, the tag has to be supplied together with the variable's address when the procedure
is called, i.e. when the actual parameter is established. Record structured VAR-parameters
therefore consist of address and type tag. This is similar to dynamic array descriptors consisting of
address and length.
0 00000020 FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF
20 00000020 00014006 FFFFFFFF FFFFFFFF FFFFFFFF
*)
MODULE Pattern14;
 TYPE
  R0 = RECORD a, b, c: INTEGER END ;
  R1 = RECORD (R0) d, e: INTEGER END ;
 VAR
  r0: R0;                                       --  40
  r1: R1;                                       --  52
PROCEDURE P(VAR r: R0);
  BEGIN ...
   r.a := 1;                                    --  LDR R1 SP 4 r
                                                --  STR R0 R1 0 r.a
   r(R1).d := 2                                 --  LDR R0 SP 8 tag(r)
                                                --  LDR R0 R0 4
                                                --  ADD R1 SB 20 R1
                                                --  CMP R2 R1 R0
                                                --  BLNE R12
                                                --  MOV R0 R0 2
                                                --  LDR R1 SP 4 r
                                                --  STR R0 R1 12 r.d
  END P; 
  BEGIN
   P(r0);                                       --  ADD R0 SB 40 r0
                                                --  ADD R1 SB 0 tag(R0)
                                                --  BL P
   P(r1)                                        --  ADD R0 SB 52 r1
                                                --  ADD R1 SB 20 tag(R1)
                                                --  BL P
END Pattern14.
