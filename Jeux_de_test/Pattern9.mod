(* Pattern 9: Function procedures. They are handled in exactly the same manner as proper
procedures, except that a result is returned in register R0. If the function is called in an expression
at a place where intermediate results are held in registers, these values are put onto the stack
before the call, and they are restored after return (not shown here).
*) 
MODULE Pattern9;
 VAR x: REAL;
 PROCEDURE F(x: REAL): REAL;
  BEGIN                           --  SUB SP SP 8
                                  --  STR LNK SP 0 push ret adr
                                  --  STR R0 SP 4 push x
   IF x >= 1.0 THEN                --  LDR R0 SP 4
                                  --  MOV' R1 R0 3F80H
                                  --  FSB R0 R0 R1
                                  --  BLT 4
     x := F(F(x))                  --  LDR R0 SP 4
                                  --  BL -9
                                  --  BL -10
                                  --  STR R0 SP 4
   END ;
   RETURN x                       --  LDR R0 SP 4
 END F;                           --  LDR LNK SP 0 pop ret adr
                                  --  ADD SP SP 8
                                  --  B R15
END Pattern9.