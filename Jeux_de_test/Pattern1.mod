(* Pattern 1: Assignment of constants. We begin with a simple example of assigning constants to
variables. The variables used in this example are global; their base register is SB. Each assignment
results in a single instruction. The constant is embedded within the instruction as a literal operand.
*)

MODULE Pattern1;
VAR ch: CHAR;                             --  0
k: INTEGER;                               --  4
x: REAL;                                  --  8
s: SET;                                   --  12
BEGIN                                     --  module entry code
ch := "0";                                --  40000030 MOV R0 R0 30H
                                          --  B0D00000 STR R0 SB 0
k := 10;                                  --  4000000A MOV R0 R0 10
                                          --  A0D00004 STR R0 SB 4
x := 1.0;                                 --  60003F80 MOV' R0 R0 3F800000H
                                          --  A0D00008 STR R0 SB 8
s := {0, 4, 8}                            --  40000111 MOV R0 R0 111H
                                          --  A0D0000C STR R0 SB 12
END Pattern1.                              -- module exit code
