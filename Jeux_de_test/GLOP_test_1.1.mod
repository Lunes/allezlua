A partir de la version 1.1, le contenu de ce fichier est considéré valide

Par convention, GlopOberon assimile toutes les lignes qui précèdent celle qui commence par le mot clef MODULE à du commentaire.
En conséquence celui-ci peut tenir sur plusieurs lignes, mêmes blanches comme ci dessous :

-----------------------
MODULE GLOP_test;   -- ignorer les espaces et les		tabulations	
----------------------- 

--PROCEDURE Proc1; 
--BEGIN
--END Proc1;
 
--BEGIN -- code d'initialisation
--  Proc1;    
----------------------
END GLOP_test_1.
----------------------
Par convention, GlopOberon ignore tout ce qui suit la fin d'un MODULE.
C'est donc pratique pour ajouter des précisions sur le code utilisé 

fic_source = io.open("GlopTest_1_1.mod") -- ouverture du fichier à compiler

repeat -- va chercher la ligne qui contient "MODULE "
  entree_position = fic_source:seek()
  ligne =   fic_source:read("l")
  i, j = string.find(ligne,"MODULE")  -- position et longueur de la chaine trouvée, donc 1 si trouvé
until  i == 1
  
ident = (string.sub(ligne, i , j)) print("on trouve : "..ident)
   
fic_source:seek("set",(entree_position+j)) -- se repositionner sur ce qui suit "MODULE "
ch = fic_source:read(1)

d'après Programming.in.Lua.4th.Edition page 109, le moyen de retrouver si un mot réservé a été détecté :

function Set (list)
 local set = {}
 for _, l in ipairs(list) do set[l] = true end
 return set
 end
mots_clefs = Set{"MODULE", "END", "VAR", "TYPE", "CONST", CHAR , INTEGER } 

if mots_clefs[ident] then print("MODULE trouvé") end
