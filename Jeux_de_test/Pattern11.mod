(* Pattern 11: Sets. This code pattern exhibits the construction of sets. If the specified elements are
constants, the set value is computed by the compiler. Otherwise, sequences of move and shift
instructions are used. Since shift instructions do not check whether the shift count is within sensible
bounds, the results are unpredictable, if elements outside the range 0 .. 31 are involved.
*)
MODULE Pattern11;
  VAR s: SET; m, n: INTEGER;
  BEGIN
    s := {m};                     --  LDR R0 SB 4 m
                                  --  MOV R1 R0 1
                                  --  LSL R0 R1 R0
                                  --  STR R0 SB 0 s
    s := {0 .. n};                --  LDR R0 SB 8 n
                                  --  MOV R1 R0 -2
                                  --  LSL R0 R1 R0
                                  --  XOR R0 R0 -1
                                  --  STR R0 SB 0
    s := {m .. 31};               --  LDR R0 SB 4 m
                                  --  MOV R1 R0 31
                                  --  MOV R2 R0 -2
                                  --  LSL R1 R2 R1
                                  --  MOV R2 R0 -1
                                  --  LSL R0 R2 R0
                                  --  XOR R0 R0 R1
                                  --  STR R0 SB 0 s
    s := {m .. n};                --  LDR R0 SB 4 m
                                  --  LDR R1 SB 8 n
                                  --  MOV R2 R0 -2
                                  --  LSL R1 R2 R1
                                  --  MOV R2 R0 -1
                                  --  LSL R0 R2 R0
                                  --  XOR R0 R0 R1
                                  --  STR R0 SB 0 s
    IF n IN {2, 3, 5, 7, 11, 13} THEN   --  MOV R0 R0 28ACH
                                  --  LDR R1 SB 8
                                  --  ADD R1 R1 1
                                  --  ASR' R0 R0 R1
                                  --  BPL 2
      m := 1                      --  MOV R0 R0 1
                                  --  STR R0 SB 4 m
    END
END Pattern11.