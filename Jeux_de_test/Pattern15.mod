Pattern 15: Array assignments and strings.
MODULE Pattern15;
VAR s0, s1: ARRAY 32 OF CHAR;
PROCEDURE P(x: ARRAY OF CHAR);
END P;
BEGIN s0 := "ABCDEF";                 --  ADD R0 SB 0 @s0
                                      --  ADD R1 SB 64 @"ABCDEF"
                                      --  LDR R2 R1 0
                                      --  ADD R1 R1 4
                                      --  STR R2 R0 0
                                      --  ADD R0 R0 4
                                      --  ASR R2 R2 24 test for 0X
                                      --  BNE -6
  s0 := s1;                           --  ADD R0 SB 0 @s0
                                      --  ADD R1 SB 32 @s1
                                      --  MOV R2 R0 8 len
                                      --  LDR R3 R1 0
                                      --  ADD R1 R1 4
                                      --  STR R3 R0 0
                                      --  ADD R0 R0 4
                                      --  SUB R2 R2 1
                                      --  BNE -6
  P(s1);                              --  ADD R0 SB 32 @s1
                                      --  MOV R1 R0 32 len
                                      --  BL -38 P
  P("012345");                        --  ADD R0 SB 72 @"012345"
                                      --  MOV R1 R0 7 len (incl 0X)
                                      --  BL -42 P
  P("%")                              --  ADD R0 SB 80 @"%"
                                      --  MOV R1 R0 2 len
                                      --  BL -46 P
END Pattern15.