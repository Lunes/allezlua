(* A type test of the form p IS T then, consists of a comparison of the type tag of p^ at address p-8
with the tag held in the descriptor of T at the extension level of the type of p^. A type guard p(T) is
synonymous to the statement
IF ~(p IS T) THEN abort END
The following example features 3 record types with associated pointer types, and hence also 3 type
descriptors. Each descriptor is 5 words long. Their addresses, and therefore their tags, are 0, 20,
and 40 respectively.
0 00000020 FFFFFFFF FFFFFFFF FFFFFFFF FFFFFFFF
20 00000020 00014006 FFFFFFFF FFFFFFFF FFFFFFFF
40 00000020 00014005 00028001 FFFFFFFF FFFFFFFF
*)
MODULE Pattern13;
TYPE
P0 = POINTER TO R0;
P1 = POINTER TO R1;
P2 = POINTER TO R2;
R0 = RECORD x: INTEGER END ;
R1 = RECORD (R0) y: INTEGER END ;
R2 = RECORD (R1) z: INTEGER END ;
VAR
p0: P0; 60
p1: P1; 64
p2: P2; 68
BEGIN
p0.x := 0; LDR R0 SB 60
MOV R1 R0 0 p0.x
STR R1 R0 0 no type check
p1.y := 1; LDR R0 SB 64
MOV R1 R0 1
STR R1 R0 4 p1.y
p0(P1).y := 3; LDR R0 SB 60 p0
LDR R1 R0 -8 tag(p0)
LDR R1 R1 4
ADD R2 SB 20 TD P1
CMP R3 R2 R1
size
pointer offsets
size
pointer offsets
size
pointer offsets
R0R2 R1
25
BLNE R12
MOV R1 R0 3
STR R1 R0 4 p0.z
p0(P2).z := 5; LDR R0 SB 60 p0
LDR R1 R0 -8 tag(p0)
LDR R1 R1 8
ADD R2 SB 40 TD P2
CMP R3 R2 R1
BLNE R12
MOV R1 R0 5
STR R1 R0 8 p0.z
IF p1 IS P2 THEN LDR R0 SB 64 p1
LDR R1 R0 -8 tag(p1)
LDR R1 R1 8
ADD R2 SB 40 TD P2
CMP R3 R2 R1
BNE 2
p0 := p2 LDR R0 SB 68
STR R0 SB 60
END
END Pattern13.
