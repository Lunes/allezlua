(* Pattern 8: Proper procedures: Procedure bodies are surrounded by a prolog (entry code) and an
epilog (exit code). They reposition the stack pointer SP (see Chapter 6), which holds the address of
the procedure activation record on the stack. The immediate value of the first instruction indicates
the space taken by variables local to the procedure, rounded up to the next multiple of 4.
Procedure calls use a branch and link (BL) instruction. Parameters are loaded into registers prior to
the call and pushed on the stack after the call. Every parameter occupies a multiple of 4 bytes. In
the case of value parameters the value is loaded, and in the case of VAR-parameters, the
variable's address is loaded.
*)
MODULE Pattern8;
 VAR i: INTEGER;
 PROCEDURE P(x: INTEGER; VAR y: INTEGER);
 VAR z: INTEGER;
 BEGIN                                      --  SUB SP SP 16 adjust SP
                                            --  STR LNK SP 0 push ret adr
                                            --  STR R0 SP 4 push x
                                            --  STR R1 SP 8 push @y
    z := x;                                 --  LDR R0 SP 4 x
                                            --  STR R0 SP 12 z
    y := z                                  --  LDR R0 SP 12 z
                                            --  LDR R1 SP 8 @y
                                            --  STR R0 R1 0 y
 END P;                                     --  LDR LNK SP 0 pop ret adr
                                            --  ADD SP SP 16
                                            --  B R15
 BEGIN P(5, i)                              --  MOV R0 R0 5
                                            --  ADD R1 SB 0 @i
                                            --  BL -14 call
END Pattern8.
