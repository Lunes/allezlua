A partir GLOP en version 1.2, le contenu de ce fichier devra toujours être considéré valide c.a.d compilable
Il respecte la grammaire EBNF suivante : 

	ident = letter {letter | digit}.
	type = ident.
	IdentList = ident {"," ident}.
	Declarations = ["VAR" {IdentList ":" type ";"}].
	module = "MODULE" ident ";" Declarations "END" ident "." 


____________________________________________________________________________________________________________
MODULE Glop_test;  -- donc le programme commence à partir d'ici

VAR
    car2 ,car3 ,car4		: CHAR;
	car1					: CHAR;
	int1 					: INTEGER;
    int2,int3 , int4		: INTEGER; -- ignorer les espaces et les		tabulations	
 
  
-----------------
--PROCEDURE Proc1; 
-----------------
-- ne fait rien ;-)

--END Proc1;

--       code d'initialisation du module    -- 
--BEGIN 
--  Proc1;    


END Glop_test.
_____________________________________________________________________________________________________________

code utilisé :

-- Constantes globales
null_ = 0
MODULE_ = 69
ident_ = 31
point_virgule_ = 52
END_ = 53
point_ = 18
virgule_ = 40
VAR_   = 65
deux_points_ = 41

-- Symboles lexicaux d'Oberon 
symboles = { 
    null_ = 0, point_ = 18, CHAR = 20, INTEGER = 21, ident_ = 31, comma_ = 40, deux_points_ = 41,
    point_virgule_ = 52 , VAR = 65 , END = 53 , MODULE = 69 ,
}


-- Variables globales
cc = ''				-- caractère courant
symbole = null_			-- symbole courant
identifiant = ''		-- identifiant courant


function Identifiant() 
... 

  if mots_clefs[identifiant] then
    io.write(" qui est un mot clef et son code est ",symboles[identifiant], ' ')
    return symboles[identifiant]
  else
    return ident_
  end
....
