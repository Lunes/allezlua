(* Pattern 12: Imported variables and procedures: When a procedure is imported from another
module, its address is unavailable to the compiler. Instead, the procedure is identified by a number
obtained from the imported module's symbol file. In place of the offset, the branch instruction holds
(1) the number of the imported module, (2) the number of the imported procedure, and (3) a link in
the list of BL instructions calling an external procedure. This list is traversed by the linking loader,
that computes the actual offset (fixup, see Chapter 6).
Imported variables are also referenced by a variable's number. In general, an access required two
instructions. The first loads the static base register SB from a global table with the address of that
module's data section. The module number of the imported variable serves as index. The second
instruction loads the address of the variable, using the actual offset fixed up by the loader.
In the following example, modules Pattern12a and Pattern12b both export a procedure and a
variable. They are referenced from the importing module Pattern12c.
*)
MODULE Pattern12a;
VAR k*: INTEGER;
PROCEDURE P*;
BEGIN k := 1
END P;
END Pattern12a.

MODULE Pattern12b;
VAR x*: REAL;
PROCEDURE Q*;
BEGIN x := 1
END Q;
END Pattern12b.

MODULE Pattern12c;
IMPORT Pattern12a, Pattern12b;
VAR i: INTEGER; y: REAL;
BEGIN
  i := Pattern12a.k;                                    --  8D10xxxx LDR SB 1 link Pattern12a
                                                        --  80D00000 LDR R0 SB 0 Pattern12a.k
                                                        --  8D00xxxx LDR SB 0 link Pattern12c
                                                        --  A0D00000 STR R0 SB 0 Pattern12c.i
  y := Pattern12b.x;                                    --  8D20xxxx LDR SB 2 link Pattern12b
                                                        --  80D00000 LDR R0 SB 0 Pattern12b.x
                                                        --  8D00xxxx LDR SB 0 link Pattern12c
                                                        --  A0D00004 STR R0 SB 4 Pattern12c.y
END Pattern12c.