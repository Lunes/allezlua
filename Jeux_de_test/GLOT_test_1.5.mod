création de constantes simples de type INTEGER, BOOLEAN , CHAR
          integer = digit  {digit}
          ident = letter {letter | digit}.
          type = ident.
          factor= ident
          IdentList = ident {"," ident}.
          Declarations = ["CONST" {ident"=" factor ";"}]
                         ["TYPE" {ident":" type ";"}]
                         ["VAR" {IdentList ":" type ";"}]
          module = "MODULE" ident ";" Declarations "END" ident "."  


MODULE GLOT_test; -- Valide à partir de GLOT version 1.5 et ultérieures
(* identique en fait à GLOP_Test_1_4 *)


CONST 	(* un commentaire multilignes :
			les constantes de type chaîne seront vus dans une version ultérieure
		*)

  const_int1	= 1;
  const_int200  = 200;
-- arobase       = '@'
-- version     = "version 5012"

(* on va corser 
              (* récursion (*   à la "Inception"  *)  *)

*)
 
TYPE  
  car_type1                       : CHAR; 
  int_type1                       : INTEGER; 
  bool_type1                      : BOOLEAN; 


VAR 
  car2 ,car3 ,car4       : CHAR;

(* types définis à voir dans une prochaine itération  
  car1                   : car_type1; 
  int1                   : int_type1; 
  int2 ,int3 , int4      : int_type2;
*)

(*
BEGIN 
*)

END GLOT_test._
