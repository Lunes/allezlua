(* Pattern 17: Predeclared functions. *)
MODULE Pattern17;
VAR m, n: INTEGER;
x, y: REAL;
b: BOOLEAN; ch: CHAR;
BEGIN
  n := ABS(m);                            --  LDR R0 SB 0 m
                                          --  CMP R0 R0 0
                                          --  BGE 2
                                          --  MOV R1 R0 0
                                          --  SUB R0 R1 R0
                                          --  STR R0 SB 4 n
  y := ABS(x);                            --  LDR R0 SB 8 x
                                          --  LSL R0 R0 1
                                          --  ROR R0 R0 1
                                          --  STR R0 SB 12 y
  b := ODD(n);                            --  LDR R0 SB 4 n
                                          --  AND R0 R0 1
                                          --  BEQ 2
                                          --  MOV R0 R0 1
                                          --  B 1
                                          --  MOV R0 R0 0
                                          --  STB R0 SB 16 b
  n := ORD(ch);                           --  LDB R0 SB 17 ch
                                          --  STR R0 SB 4 n
  n := FLOOR(x);                          --  LDR R0 SB 8 x
                                          --  MOV' R1 R0 4B00H
                                          --  FAD" R0 R0 R1 floor
                                          --  STR R0 SB 4 n
  y := FLT(m);                            --  LDR R0 SB 0 m
                                          --  MOV' R1 R0 4B00H
                                          --  FAD' R0 R0 R1 float
                                          --  STR R0 SB 12 y
  n := LSL(m, 3);                         --  LDR R0 SB 0 m
                                          --  LSL R0 R0 3
                                          --  STR R0 SB 4 n
  n := ASR(m, 8);                         --  LDR R0 SB 0
                                          --  ASR R0 R0 8
                                          --  STR R0 SB 4
  m := ROR(m, n);                         --  LDR R0 SB 0
                                          --  LDR R1 SB 4
                                          --  ROR R0 R0 R1
                                          --  STR R0 SB 0
END Pattern17.
