-- MODULE OSS (d'apr�s Programming in Lua 4th Chapitre 17 )

local IdLen = 64
local null_ = 0
local module_ = 69
local ident_ = 31
local semicolon_ = 52
local end_ = 53
local period_ = 18
local comma_ = 40



--------------------------
local function Init(parametre) 
--------------------------
fic_source = parametre

end


--------------------------
local function Set (list)
--------------------------
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set 
end

local mots_clefs = Set{"MODULE", "END", "VAR", "TYPE", "CONST", CHAR , INTEGER }    

--------------------------
local function Erreur(message)
--------------------------
  print("Erreur : "..message.." et symbole a pour valeur : ",symbole)
  fic_source:close()
  os.exit(1)

end 

--------------------------------
local function Check(attendu, message)
--------------------------------
 io.write("Check ", attendu," ")
 if message == nil then message = 'attendu' else io.write(message) end
 if symbole ~= attendu then Erreur(message) end
 io.write(" ok ")
 Get(symbole)
 return true
end 

--------------------------
local function Number() 
--------------------------
  io.write("----Number() ")
  nombre = '';
  while ( (cc >= '0' and cc <= '9'))  do 
    nombre = nombre..cc 
    cc = fic_source:read(1) 
  end 
  io.write(" => "..nombre.. " ")
  return entier_
end 


--------------------------
local function chaine()
--------------------------
  const_chaine = ''
  repeat 
    cc = fic_source:read(1) 
    const_chaine = const_chaine..cc 
   until cc == '"'
 end 

--------------------------
local function Identifiant() 
--------------------------
  io.write("----Identifiant() ")
  identifiant_prec = identifiant
  identifiant = '';
  while ( cc ==  '_' or (cc >= '0' and cc <= '9') or (cc >= 'A' and cc <= 'Z') or (cc >= 'a' and cc <= 'z'))  do 
    identifiant = identifiant..cc 
    cc = fic_source:read(1) 
  end 
  io.write(" => "..identifiant.. " ")
  if mots_clefs[identifiant] then
    io.write(" qui est un mot clef et son code est ", symboles[identifiant], ' ')
    return symboles[identifiant]
  else
    return ident_
  end
end 

-----------------------------------
local function Commentaire_multilignes()
-----------------------------------
  io.write(" Commentaire_multilignes ")
  io.write(cc)
  repeat
    repeat 
      cc = fic_source:read(1) io.write(cc)
      while cc == '(' do 
	    cc = fic_source:read(1) io.write(cc)
        if cc == '*' then io.write("\nr�cursion\n__________") Commentaire_multilignes() end -- r�cursion car commentaires imbriqu�s
      end
	until cc == '*'
    repeat cc = fic_source:read(1) io.write('~') until cc ~= '*'
  until  (cc == ')') 
--IF ~R.eot THEN Texts.Read(R, ch) ELSE Mark("comment not terminated") END
  cc = fic_source:read(1)
  return null_
end --Commentaire_multilignes

--------------------------
local function Commentaire_fin_ligne()
--------------------------
  io.write(" Commentaires_une_ligne ")
  io.write(fic_source:seek())
  ligne =   fic_source:read("l") 
     
  io.write("#"..ligne.."#")
  io.write(fic_source:seek())
  cc = fic_source:read(1)
  io.write("\n")
  return null_
end -- Commentaire_fin_ligne


--------------------------
local function Get(nouveau_symbole) 
--------------------------
  io.write("\n-- GetSymbole() ")
  repeat 
    while cc <=  '\32' do io.write("{"..cc)  cc = fic_source:read(1) end    --  elimine CR, LF, TAB, SPACE
    io.write("le dernier caractere lu est '"..cc.."' ")
	if (cc == '_' or (cc >= 'A' and cc <= 'Z') or (cc >= 'a' and cc <= 'z')) then symbole = Identifiant(); break end  
	if (cc >= '0' and cc <= '9') then symbole = Number();				 break end
	if cc == ';' then cc = fic_source:read(1) symbole = point_virgule_	 break end  
	if cc == '.' then cc = fic_source:read(1) symbole = point_			 break end
	if cc == ':' then cc = fic_source:read(1) symbole = deux_points_	 break end
	if cc == ',' then cc = fic_source:read(1) symbole = virgule_    	 break end 
	if cc == '=' then cc = fic_source:read(1) symbole = egale_    	     break end	
	if cc == '"' then cc = fic_source:read(1) symbole = guillemet_ 	     break end
    if cc == '-' then 
      cc = fic_source:read(1)
      if cc == '-' 
	    then symbole = Commentaire_fin_ligne() 
	    else symbole = moins_
	  end 
    end
    if cc == '(' then 
	  io.write("Parenthese ouvrante ")
      cc = fic_source:read(1)
      if cc == '*' 
	    then symbole = Commentaire_multilignes() 
	    else symbole = parenthese_ouvrante_
	  end 
    end
	io.write('pas break�')
  until symbole ~= null_    -- caract�re_suivant
  io.write("\n-- exit GetSymbole() avec valeur ",symbole, cc, "\n")
  return symbole
end 




--------------------------
--EXPORT
--------------------------

return {
  IdLen 		= IdLen ,
  null_ 		= null_ ,		
  module_		= module_ ,	
  ident_		= ident_ ,		
  semicolon_ 	= semicolon_ ,   
  end_ 		= end_ ,	
  period_ 		= period_ , 	
  virgule 		= comma_ ,		
  Erreur		= Erreur ,
  mots_clefs	= mots_clefs , 
  Get		= Get ,
  Set		= Set,
  Init		= Init,
}

--END MODULE OSS