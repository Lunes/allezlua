--MODULE Serial_OUT
-- version du 13/07


--To insert an element at the beginning of the list, with a value v, we do

--list = nil
for v = 26, 1, -1 do
    list = {next = list, value = v, name = string.char(v+64)}

end


--To traverse the list, we write:
    local l = list
    while l do
    -- print(l.value, l.name)
      l = l.next
    end

--[[

Object = POINTER TO ObjDesc; 
ObjDesc = RECORD 
 name: Ident; 
 class: INTEGER; 
 type: Type; 
 next: Object; 
 val: LONGINT 
END

Type = POINTER TO TypDesc; 
TypDesc = RECORD 
 form, len: INTEGER; 
 fields: Object; 
 base: Type 
END

--]]


--[

--]]


function newObject(nom,classe,nature,value,base)
 print("newObject : ",nom,classe,nature,value,base)
 ObjectList = {name = nom , class = classe, natur = nature ,  val = value, bas = base , next = ObjectList}
 print(ObjectList)
end -- newObject

function newType(f,l,b, fi)
  print("newType : ",f,l,b, fi)
  if f == "RECORD" then  
    for k,v in ipairs(b) do  fi = newObject(ObjectList,k,"FIELD",v) end
  end
  Type = {next = Type, form = f , len = l,  fields = fi, base = b,}
  return Type
end -- newType

--[[
CONST N = 10; 
TYPE T = ARRAY N OF INTEGER; 
VAR x, y: T
--]]

ObjectList = nil  -- point d'entrée niveau MODULE
newObject("N","CONST","INTEGER",10)
newObject("T","TYPE","ARRAY",10, "INTEGER")
newObject("R","TYPE","RECORD", {f="INTEGER",g="INTEGER",tableau="T"})
newObject("x","VAR","INTEGER")
newObject("y","VAR","T")


--[[
TYPE 
  R = RECORD f, g: INTEGER END ; 
VAR 
  x: INTEGER; 
  a: ARRAY 10 OF INTEGER; 
  r, s: R; 
 
--]]




	print ("\nObjects : ")
    local l = ObjectList
    while l do
	  if l.natur == "RECORD"
	    then  print(l , l. next , l.name , l.class , l.natur , "champs : "); for k,v in pairs(l.val) do  print(k,v) end
	    else  print(l , l. next , l.name , l.class , l.natur , l.val)
	  end
      l = l.next
    end
	
	print ("\nTypes : ")
    local l = Type
    while l do
	  print(l , l.next , l.form , l.len , l.fields, l.base  )
	  --if l.fields ~= nil then io.write("not null ",l.fields) end
      l = l.next
    end
	print()
	


 

--END Serial_OUT
