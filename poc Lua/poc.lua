--lexical symbols 
local  module = 69 end_ = 53 var = 65 type_ = 64 const = 63 char = 20 int = 21 if_ = 32 while_ = 34 repeat_ = 35 case = 36 for_ = 37
local real = 22 false_ = 23 true_ = 24
local then_ = 47 of = 48 do_ = 49 and_ = 5 in_ = 15 is = 16 to = 50 by = 51 
local nil_ = 25 mod = 4 else_ = 55 elsif_ = 56 until_ = 57 return_ = 58
local not_ = 27 array = 60 record = 61 pointer = 62 
local  procedure = 66 begin = 67 import = 68
    
local null = 0 times = 1 rdiv = 2 div = 3 
local plus = 6 minus = 7 or_ = 8 eql = 9
local neq = 10 lss = 11 leq = 12 gtr = 13 geq = 14
local arrow = 17 period = 18
local string = 26 lparen = 28 lbrak = 29
local lbrace = 30 ident = 31
local comma = 40 colon = 41 becomes = 42 upto = 43 rparen = 44
local rbrak = 45 rbrace = 46 
local semicolon = 52 bar = 54 eot = 70

local eof = false

local tokens = {["MODULE"]=module, ["END"]=end_, ["VAR"]=var, ["TYPE"]=type_, ["CONST"]=const, ["CHAR"]=char, ["INTEGER"]=int, ["BEGIN"]=begin, [";"]=semicolon, [':']=colon, ["."]=period, [","]=comma, ["="]=eql, [':=']=becomes}


local function Set (list)
--------------------------
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set 
end


local mots_clefs = Set{"MODULE", "END", "VAR", "TYPE", "CONST", "CHAR" , "INTEGER", "ARRAY", "OF", "RECORD" , "BEGIN" }  


local function Huit (list)
--------------------------
  local set = {}
  local i = 1
  for _, l in ipairs(list) do 
    set[i] = l 
    i = i + 1
  end
  return set 
end

local mots_reserves = Huit{"MODULE", "END", "VAR", "TYPE", "CONST", "CHAR" , "INTEGER", "ARRAY", "OF", "RECORD" , "BEGIN" }  

-- local ponctuation = {[";"]=semicolon, ["."]=colon, [","]=comma, ["="]=eql, [':=']=becomes }

-- local ponctuation = Huit{[";"]=semicolon, ["."]=colon, [","]=comma, ["="]=eql, [':=']=becomes }

local operateurs_ulterieurs = {['<']=1, ['>']=2, ['<=']=3, ['>=']=4, ['^']=5, ['<>']=6, ['..']=7, ['(']=8, [')']=9, '+', '-', '*', '/'} 

---------------------------------------
local function Obligatoire(attendu)
  ---------------------------------------
  local trouve = tokens[attendu] == symbole
  --print('\nobligatoire attend '..attendu..' et va renvoyer ', trouve)
  return '\nobligatoire cherche '..attendu..' et renvoie : ', trouve
end
  
  
print(tokens['MODULE'])
print(tokens[';'])

symbole = tokens[':=']
print(Obligatoire(':='))
print(Obligatoire('=='))


 for i, l in ipairs(mots_reserves) do 
    print(mots_reserves[i])
  end
  
  mots_reserves[return_]="RETURN"
  
--print(tokens['MODULE'].." : "..mots_reserves[tokens['MODULE']])


local ponctuation = {";", ".", ",", "=", ":="}
ponctuation[6]="~="

 for i, l in ipairs(ponctuation) do 
    print(i..ponctuation[i])
  end

