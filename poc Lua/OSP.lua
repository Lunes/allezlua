-- MODULE OSP (d'apr�s Programming in Lua 4th Chapitre 17 )
local OSS = require "OSS"

--print(OSS.semicolon_ , OSS.comma_)


--for k,v in pairs(OSS) do print(k,v) end

OSS.virgule = 99
--print(OSS.comma_)



print("module OSS charg� ",OSS.mots_clefs["END"])


--------------------------
function Type()
--------------------------
 io.write("\nType ...")
   if symbole == ident_ then
    if type_de_base[identifiant] ~= true then Erreur(identifiant.." est de type non d�fini") end
  end
  OSS.Get(symbole)
end   -- Type

--------------------------
function IdentList() 
--------------------------
  io.write("\nIdentList ...") 
  if symbole == ident_ then 
    OSS.Get(symbole)
	while symbole == virgule_ do OSS.Get(symbole) Check(ident_,'identifiant') end
  end
  Check(deux_points_, "deux points")
   
end   -- IdentList

--------------------------
function Declarations()
--------------------------
  io.write("Declarations() ")
  if symbole == CONST then 
    io.write("CONST ...")
	OSS.Get(symbole)
	while symbole == ident_ do
	 OSS.Get(symbole)
	 Check(egale_, "�gale")
	 Number()
	 Check(entier_,"entier")
     Check(point_virgule_, ';') 
	end
  end

  if symbole == TYPE then 
    io.write("TYPE ...")
    OSS.Get(symbole)
	while symbole == ident_ do
	 OSS.Get(symbole)
	 Check(deux_points_, "deux points")
	 Type()
     Check(point_virgule_, ';') 
	end
  end

  if symbole == VAR_ then 
  io.write("VAR ...")
  OSS.Get(symbole)  
    while symbole == ident_ do
	  IdentList() 
	  Type()
	  Check(point_virgule_, ';') 
    end
  end  

  io.write('\n') 

end 

--------------------------
function Module() 
--------------------------
  Check(ident_, "identifiant")
  local module_identifiant = identifiant; print("module_identifiant : "..module_identifiant)
  Check(point_virgule_, ';')
  Declarations()
  Check(END_,"END")
  if identifiant ~= module_identifiant then Erreur(identifiant.." ne correspond pas avec l'identifiant du module : "..module_identifiant) end
  OSS.Get(symbole)
  Check(point_ , '.')
  print("compilation r�ussie")
end --Module

--------------------------
function Compile(fic_source) 
--------------------------
print(fic_source)
  repeat -- va chercher la ligne qui contient "MODULE "
     entree_position = fic_source:seek()
     ligne =   fic_source:read("l")
     i, j = string.find(ligne,"MODULE")
     --print(ligne)
  until  i == 1
  identifiant = (string.sub(ligne, i , j)) print("on a trouv� : "..identifiant)
   
  fic_source:seek("set",(entree_position+j))  cc = fic_source:read(1) -- se repositionner sur ce qui suit "MODULE "
  OSS.Get(symbole)
  Module()   

end -- Compile   


--------------------------
--EXPORT
--------------------------

return {
  Compile = Compile                              
}

--END MODULE OSS