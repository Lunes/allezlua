-- MODULE OSP 
version = "'GLOP' Go Lua Oberon Parser :  version 1.7 (Etape 1, iteration 7) version en cours du 24 novembre 2024"

--local strict = require 'std.strict'
local OSS = require "GLOS"
local meme_type = {}
--print("module OSS chargé ",OSS.mots_clefs["END"])
--for k,v in pairs(OSS) do print(k,v) end


--local ponctuation = {[OSS.semicolon] = ';' , [OSS.end_] = 'END' , [OSS.colon] = '.' , [8] = ':' }

function values(t) -- cf PIL p172
  local i = 0
  return function() i = i + 1; return t[i] end
end

-------------------------------------------------
local function newObject(nom,classe,nature,value,base)
-------------------------------------------------
 ObjectList = {name = nom , class = classe, natur = nature ,  val = value, bas = base , next = ObjectList}
  print("newObject : ",nom,classe,nature,value,base,ObjectList)
end -- newObject

----------------------------
function newType(f,l,b, fi)
----------------------------
  print("newType : ",f,l,b, fi)
  if f == "RECORD" then  
    for k,v in ipairs(b) do  fi = newObject(ObjectList,k,"FIELD",v) end
  end
  return {next = OSS.Type, form = f , len = l,  fields = fi, base = b,}
end -- newType

---------------------------------------
local function Check(attendu, message)
---------------------------------------
 if symbole ~= attendu then OSS.Erreur(message) end -- ne reviendra pas car la compilation est finie
 return true
end 


---------------------------------------

local function Obligatoire(attendu)
  ---------------------------------------
  io.write('GLOP.Obligatoire : '..attendu..' ')
  local trouve = OSS.tokens[attendu] == symbole
  if trouve == false then OSS.Erreur(attendu) end     -- ne reviendra pas car la compilation est finie
  -- tout va bien , on va chercher le symbole suivant 
  io.write(' a été trouvé, parfait !\n') 
  OSS.Get(symbole)
  return trouve  
end

---------------------------------------
local function Facultatif(attendu)
---------------------------------------
  return OSS.tokens[attendu] == symbole
end
----
--------------------------
function _3_Type()
--------------------------
 io.write("_3_Type (recherche du type)...\n")
   if symbole == OSS.ident then
    if type_de_base[identifiant] ~= true then 
      OSS.Erreur(identifiant.." est de type non défini") end
  end
 io.write("le type est "..identifiant..'\n')
 for chaque_variable in values(meme_type) do -- pour chaque élément de la liste construite par _3_IdentList
   newObject(chaque_variable,"VAR",identifiant, nil)
  end
end

--------------------------
function _3_IdentList() 
--------------------------
  io.write("_3_IdentList ...\n") 
  meme_type = {}
  while ( true ) do      --
    Check(OSS.ident,"la référence à un identifiant est attendue")
    table.insert(meme_type,identifiant)
    OSS.Get(symbole)
    if symbole == OSS.colon then OSS.Get(symbole) break end
    Obligatoire(',')
  end -- on va chercher la prochaine occurrence
  
end   -- _3_IdentList


function _4_VariableDeclaration()
  local chaque_variable
  io.write("_4_VariableDeclaration ...\n")
  OSS.Get(symbole) 
  repeat 
    _3_IdentList()
    _3_Type()
    OSS.Get(symbole) ; Obligatoire(';')
  until symbole ~= OSS.ident
end


---------------------------------
function _4_TypeDeclaration()
  ---------------------------------

  
  io.write("_4_TypeDeclaration ...\n")
  OSS.Get(symbole)
  while symbole == OSS.ident do
    OSS.Get(symbole)
    Obligatoire('=',"séparateur égale attendu")
    _3_Type()
    OSS.Get(symbole); Obligatoire(';',"séparateur point virgule attendu") 
	end
end
---------------------------------
function _4_ConstantDeclaration()
---------------------------------
  io.write("_4_ConstantDeclaration()\n")
  if OSS.Get(symbole) == OSS.ident then
    repeat
     OSS.Get(symbole); Obligatoire('='); 
     OSS.Get(symbole); Check(OSS.int,"entier")
     OSS.Get(symbole); Obligatoire(';')
     -- définition ok , on crée l'objet
     newObject(identifiant_prec,"CONST","INTEGER",nombre)
    until OSS.Get(symbole) ~= OSS.ident
  end 
end



--------------------------
function Instructions()
  --------------------------
  OSS.Get(symbole) Check(OSS.ident, "identifiant")
  --OSS.Get(symbole) 
  Check(OSS.affectation, "affectation")
  OSS.Get(symbole) Check(OSS.ident, "identifiant")
  Check(OSS.point_virgule, ';')
end --Instructions


function WhileStatement()
  -- WHILE expression DO StatementSequence {ELSIF expression DO StatementSequence} END.
end

function RepeatStatement()
  -- = REPEAT StatementSequence UNTIL expression.

end

function ForStatement()
  -- = FOR ident ":=" expression TO expression [BY ConstExpression] DO StatementSequence END.
end

------------------------------------
function _8_ProcedureHeading()
  --_8_ProcedureHeading = PROCEDURE _2_identdef [_7_FormalParameters].
------------------------------------
  -- PROCEDURE 
  --identdef()
  --  [FormalParameters].
end

------------------------------------
function _11_StatementSequence()
    while symbole ~= OSS.end_ do
     OSS.Get(symbole)
    end
end

------------------------------------
function _13_ProcedureDeclaration() 
-- _13_ProcedureDeclaration = _8_ProcedureHeading ";" _15_ProcedureBody _1_ident.
------------------------------------
  _8_ProcedureHeading()
  --Check(";") 
  _15_ProcedureBody();
  --Check(OSS.ident)
end

--------------------------
function _14_DeclarationSequence()
--_14_DeclarationSequence = [CONST {_7_ConstDeclaration ";"}] [TYPE {_4_TypeDeclaration ";"}][VAR {_4_VariableDeclaration ";"}]{_13_ProcedureDeclaration ";"}.
--------------------------
    io.write("_14_DeclarationSequence()\n")
    if Facultatif('CONST')     then _4_ConstantDeclaration()    end
    if Facultatif('TYPE')      then _4_TypeDeclaration()        end
    if Facultatif('VAR')       then _4_VariableDeclaration()    end
    if Facultatif('PROCEDURE') then _13_ProcedureDeclaration()  end
    io.write('fin DeclarationSequence\n') 
  
  end 

--------------------------
function _15_ProcedureBody()
  --------------------------
  --_14_DeclarationSequence() 
    --[BEGIN StatementSequence] [RETURN expression] END.
  end
  

------------------------------------------
function _15_import()
-- _15_import = _1_ident [":=" _1_ident].
------------------------------------------
end

--------------------------
function _16_ImportList()
--_16_ImportList = IMPORT _15import {"," _15import} ";".
--------------------------
end
----------------------
function _17_Module() 
--------------------------
-- _17_Module = MODULE _1_ident ";" [_16_ImportList] _14_DeclarationSequence [BEGIN _11_StatementSequence] END _1_ident "." .

  Check(OSS.ident, "identifiant")
  local module_identifiant = identifiant; OSS.Get(symbole);
  Obligatoire(';')
  if Facultatif('IMPORT') then _16_ImportList() end
 _14_DeclarationSequence()
  if Facultatif('BEGIN')  
   then _11_StatementSequence() 
  end
  Obligatoire('END');
  if identifiant ~= module_identifiant 
   then OSS.Erreur("Le nom du module ne correspond pas à celui défini au départ "..module_identifiant.." ~= "..identifiant ) 
  end
 print(identifiant)
 OSS.Get(symbole)
 Obligatoire('.')
end --Module

-----------------------------
function Compile(fic_source) 
-----------------------------

  OSS.Get(symbole)
  Obligatoire('MODULE');
 _17_Module() 
 print('Glop Glop')  
end -- Compile   


--------------------------
--EXPORT
--------------------------

return {
  version   = version ,   
  Compile   = Compile , 
  newObject = newObject , 
}

--END MODULE OSP
