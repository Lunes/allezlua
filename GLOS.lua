local version = "'GLOS' Go Lua Oberon Scanner :  version 1.7 (Etape 1, iteration 7, version en cours) du 24 novembre 2024"
-- Symboles lexicaux d'Oberon 


--lexical symbols 
local ident  = 31 eof = false  null = 0 
local module = 69 end_ = 53 var = 65 type_ = 64 const = 63 char = 20 int = 21 local  procedure = 66 begin = 67 import = 68
local comma  = 40 colon = 41 becomes = 42 semicolon = 52  period = 18

local tokens = {["MODULE"]=module, ["END"]=end_, ["VAR"]=var, ["TYPE"]=type_, ["CONST"]=const, ["CHAR"]=char, ["INTEGER"]=int, ["BEGIN"]=begin, [";"]=semicolon, [':']=colon, ["."]=period, [","]=comma, ["="]=eql, [':=']=becomes}



--local if_ = 32 while_ = 34 repeat_ = 35 case = 36 for_ = 37
--local real = 22 false_ = 23 true_ = 24
--local then_ = 47 of = 48 do_ = 49 and_ = 5 in_ = 15 is = 16 to = 50 by = 51 
--local nil_ = 25 mod = 4 else_ = 55 elsif_ = 56 until_ = 57 return_ = 58
--local not_ = 27 array = 60 record = 61 pointer = 62 
--times = 1 rdiv = 2 div = 3 
--local plus = 6 minus = 7 or_ = 8 eql = 9
--local neq = 10 lss = 11 leq = 12 gtr = 13 geq = 14
--local arrow = 17
--local string = 26 lparen = 28 lbrak = 29
--local lbrace = 30  upto = 43 rparen = 44
--local rbrak = 45 rbrace = 46 
--local bar = 54 eot = 70


--local ponctuation = {[";"]=semicolon, ["."]=colon, [","]=comma, ["="]=eql, [':=']=becomes }
              

  --local operateurs_ulterieurs = {['<']=, '>', '<=', '>=', '^', '<>', '..', '(', ')', '+', '-', '*', '/'} 

--------------------------
local function Set (list)
--------------------------
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set 
end

-- local mots_clefs = Set{"MODULE", "END", "VAR", "TYPE", "CONST", "CHAR" , "INTEGER", "ARRAY", "OF", "RECORD" , "BEGIN" }  

--------------------------
local function Erreur(message)
--------------------------
  print("Pas glop : "..message.." et symbole : ",symbole)
  fic_source:close()
  os.exit(1)
end 

------------------------------
local function carsuiv()
--------------------------------
  cc = fic_source:read(1) 
  if cc == nil then cc = "fin de fichier atteinte" ; eof = true end
  --io.write(cc)
end

--------------------------
local function Number() 
--------------------------
    nombre = '';
    while ( (cc >= '0' and cc <= '9'))  do 
      nombre = nombre..cc 
      carsuiv() 
    end 
    io.write(" nombre de type INTEGER détecté => "..nombre.." ")
    return int
end 
  
-----------------------------------
local function Commentaire_multilignes()
-----------------------------------
  repeat
    repeat 
      carsuiv() 
      while cc == '(' do 
	      carsuiv() 
        if cc == '*' then Commentaire_multilignes() end -- récursion car commentaires imbriqués
      end
   	until cc == '*'
    repeat carsuiv() until cc ~= '*'
  until  (cc == ')') 
  carsuiv()
  --print('fin Commentaire_multilignes')
  return null
end 

--------------------------
local function Commentaire_fin_ligne()
--------------------------
  ligne =   fic_source:read("l") 
  return null
end 

--------------------------
local function Identifiant() 
--------------------------
  io.write("GLOS.Identifiant() =>  ")
  identifiant_prec = identifiant
  identifiant = '';
  while  (eof == false) and (( cc ==  '_' or (cc >= '0' and cc <= '9') or (cc >= 'A' and cc <= 'Z') or (cc >= 'a' and cc <= 'z')))  do 
    identifiant = identifiant..cc 
    carsuiv() 
  end 
  io.write(identifiant)
  -- UTILISER MOTS_CLEFS
  if tokens[identifiant] ~= nil then
     print(" qui est un mot clef")
    return tokens[identifiant]
  else
    io.write('\n')
    return ident
  end
end 

-------------------------
local function Get(dummy)   
-- symbole est une variable globale 
  io.write('OSS.Get => ')
  repeat 
    --print('Get() cc ', cc)
    while ( cc <=  '\32' and eof == false) do carsuiv() end --  élimine CR, LF, TAB, SPACE
    symbole = null
    if (cc == '_' or (cc >= 'A' and cc <= 'Z') or (cc >= 'a' and cc <= 'z')) then 
      symbole = Identifiant(); 
      break 
    end
	  
    if (cc >= '0' and cc <= '9') then symbole = Number() break end
	  if cc == ';' then carsuiv() symbole = semicolon	     break end  
	  if cc == '.' then carsuiv() symbole = period		     break end
	  if cc == '=' then carsuiv() symbole = eql   	       break end	
	  if cc == ',' then carsuiv() symbole = comma    	     break end 
	  if cc == '"' then carsuiv() symbole = guillemet 	   break end
    
    if cc == ':' then carsuiv() 
                      if cc == '=' then carsuiv(); symbole = becomes 
                                   else            symbole = colon
                      end;
                      break               
    end
    
	  if cc == '-' then 
      carsuiv()
      if cc == '-' 
	     then symbole = Commentaire_fin_ligne() 
	     else symbole = moins ; break
	    end
    end
    if cc == '(' then 
      carsuiv()
      if cc == '*' 
	     then symbole = Commentaire_multilignes();
	     else symbole = parenthese_ouvrante ; break
      end 
    end
    carsuiv()
  until (symbole > null)  -- s'il y a eu des commentaires on passe au caractère_suivant

  --print(symbole..' '..cc)
  return symbole
end 


--------------------------
--EXPORT
--------------------------
 
 
return {
  version		  = version ,
  IdLen 		  = IdLen ,
  ident 		  = ident ,
  
  null 		  	= null ,		
  module		  = module ,	
  end_		  	= end_,
  var       	= var ,
  type      	= type ,
  const			  = const ,
  char      	= char ,
  int		      = int , 
  begin     	= begin ,
  
  semicolon		= semicolon ,
  colon			  = colon  ,
  comma     	= comma ,
  eql       	= eql ,
  becomes   	= becomes ,
 
  tokens    	= tokens ,
  Erreur		  = Erreur ,
  Get		      = Get ,
  Set		      = Set ,
  Init	    	= Init ,
  eof         = eof,
}

--END MODULE GLOS
