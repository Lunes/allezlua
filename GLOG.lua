--MODULE OSG
local version = "'GLOG' Go Lua Oberon Generator :  version 1.7 (Etape 1, iteration 7, version en cours) du 5 décembre 2023"

local function Addop()
  return "Addop"
end

local function Import()
  print ("\nObjects : ")
  local l = ObjectList
  while l do
	if l.natur == "RECORD" 
	 then  print("RECORD : ",l , l. next , l.name , l.class , l.natur , "champs : "); for k,v in pairs(l.val) do  print(k,v) end
	 else  print(l , l. next , l.name , l.class , l.natur , l.val)
	end
      l = l.next
  end

end -- Import


-- EXPORT

return {
  version = version ,
  Addop = Addop	 ,
  Import = Import ,
	 
}	 

--END OSG
